**This project creates an IP core for the spatial filter, which resides in the FPGA PL Part**

**The PS Part of the project is not completed yet.**

**1. linebuffer**

A RAM model to store values for Convolution. A FIFO concept is not applicabnle here, as same values are needed more than once. 

i_data : take 8 bits of data as input

o_data : outputs 3 * 8 bits i.e. 3 elements of a single row for Convolution 

    type memory_reg is array(511 downto 0) of std_logic_vector (7 downto 0); // memory space for 1 line buffer 

**2. convolution**

Convolution operation for a 3 * 3 kernel. Currently, using the Box Blur Kernel. https://en.wikipedia.org/wiki/Kernel_(image_processing)



i_pixel_data : 3 line bufer output => 3 * 24  = 72 bits 

o_conv_data  : Conv output of 72 bits gives 1 output => 8 bits  

    type kernel is array(8 downto 0) of std_logic_vector(7 downto 0); // keeping 8-bit for future compatability 

**3. controlLogic:**

i_pixel_data : 8 bit data from the top interface 

o_pixel_data : 3 parallel line buffers (8*(3 x3)) = 72 bits

    type localBus  is array(3 downto 0) of std_logic_vector(23 downto 0); // local bus to store 3+1 line bufers. 
