----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 11/25/2021 02:27:31 PM
-- Design Name: 
-- Module Name: controlLogic - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

-- 3rd module 
entity entity_controlLogic is
     generic(KERNEL_MASK : integer := 3;    -- for a 3x3 kernel
             ACTIVE_BUFF : integer := 3;
             PIXEL_BITS  : integer := 8;
             NUM_PIXELS  : integer := 512); -- 1 line Buffer width
     Port (i_clk         : in std_logic;
           i_rst         : in std_logic;
           i_pixel_data  : in std_logic_vector(7 downto 0);
           i_data_valid  : in std_logic;
           o_pixel_data  : out std_logic_vector((KERNEL_MASK* PIXEL_BITS * ACTIVE_BUFF)-1 downto 0); -- 72 bits
           o_data_valid  : out std_logic
      );
end entity_controlLogic;

architecture Behavioral of entity_controlLogic is
    
    signal l_rdValid           : std_logic := '0'; 
    signal wrCounter           : unsigned(8 downto 0)  := (others => '0'); -- for 512 bits
    --signal wrCounter           : integer range 0 to 511; -- for 512 bits
    signal rdCounter           : unsigned(8 downto 0)  := (others => '0'); -- for 512 bits
    signal totalPixelCounter   : unsigned(10 downto 0) := (others => '0'); -- NUM_PIXELS * KERNAL 
    signal currentWrLineBuffer : unsigned(1 downto 0)  := (others => '0');
    signal currentRdLineBuffer : unsigned(1 downto 0)  := (others => '0');
    signal mux_lineBufferWr    : unsigned(3 downto 0)  := (others => '0');
    signal mux_lineBufferRd    : unsigned(3 downto 0)  := (others => '0');
    type localBus  is array(3 downto 0) of std_logic_vector(23 downto 0);
        signal lb_data  : localBus;
    type rd_state is (IDLE, READ_MODE);
        signal state : rd_state := IDLE;
    
    
begin

o_data_valid <= l_rdValid;

-- 3a  -- 
stateMachine_rd: process(i_clk)
begin
    if rising_edge(i_clk) then
        case state is 
            when IDLE =>  -- sit idle till we have 512*3 bits to read
                if (totalPixelCounter >= (NUM_PIXELS-1)*KERNEL_MASK) then
                    state <= READ_MODE;
                    l_rdValid <= '1';
                end if;
            when READ_MODE => 
                if rdCounter  = NUM_PIXELS -1 then
                    state <= IDLE;   -- this time the totalCounter only needs to count 512 more   
                    l_rdValid <= '0';
                end if;
        end case;
    end if;
end process;

-- 3b --
total_PixelCounter: process(i_clk)
begin
    if rising_edge(i_clk) then
        if i_rst ='1' then
            totalPixelCounter <= (others => '0');
        elsif(i_data_valid = '1' and l_rdValid = '0') then
            totalPixelCounter <= totalPixelCounter + 1;
        elsif(i_data_valid = '0' and l_rdValid = '1') then
            totalPixelCounter <= totalPixelCounter - 1; 
        -- when both wr and rd are valid, totalPixelCounter remains the same.
       end if;           
    end if;

end process;

 --1--
writeCounter: process(i_clk)
begin
    if rising_edge(i_clk) then
        if i_rst = '1' then
            wrCounter <= (others => '0');
         elsif i_data_valid = '1' then
            wrCounter <= wrCounter + 1;
        end if;
    end if;
end process;

-- 2--
writeLineBuffer: process(i_clk)
begin
    if rising_edge(i_clk) then
        if i_rst = '1' then
            currentWrLineBuffer <= (others => '0');
        elsif (wrCounter = NUM_PIXELS -1 and i_data_valid = '1') then
            currentWrLineBuffer <= currentWrLineBuffer + 1;
        end if;
    end if;
end process;

MUX_lineBuff: process(i_clk)
    variable tmp : integer := 0;
begin
    if rising_edge(i_clk) then
        tmp := to_integer(currentWrLineBuffer);
        mux_lineBufferWr <= (others => '0');
        mux_lineBufferWr(tmp) <= i_data_valid ;
    end if; 
    
 end process;

-- 4 --
readCounter: process(i_clk)
begin
    if rising_edge(i_clk) then
        if i_rst = '1' then
            rdCounter <= (others => '0');
        elsif(l_rdValid = '1') then     -- operated from state machine
            rdCounter <= rdCounter + 1;
        end if;
    end if;
end process;

-- 5--
readLineBuffer: process(i_clk)
begin
    if rising_edge(i_clk) then
        if i_rst = '1' then
            currentRdLineBuffer <= (others => '0');
        elsif (rdCounter = NUM_PIXELS -1 and l_rdValid = '1') then
            currentRdLineBuffer <= currentRdLineBuffer + 1;
        end if;
    end if;
end process;

-- 6 --
MUX_output: process(i_clk)
begin
   if rising_edge(i_clk) then
        if i_rst = '1' then
            o_pixel_data <= (others => '0');
        else 
            case to_integer(currentRdLineBuffer) is
                when 0 =>
                    o_pixel_data  <= lb_data(2) & lb_data(1) & lb_data(0); 
                when 1 =>
                    o_pixel_data  <= lb_data(3) & lb_data(2) & lb_data(1); 
                when 2 =>
                    o_pixel_data  <= lb_data(0) & lb_data(3) & lb_data(2); 
                when 3 =>
                    o_pixel_data  <= lb_data(1) & lb_data(0) & lb_data(3);
                when others =>
                    o_pixel_data <= (others => '1');
                
            end case;
        end if;
    end if;
end process;

-- 7 -- -- choosing which 3 line buffers to read from
MUX_readLines_valid: process(i_clk)
begin
    if rising_edge(i_clk) then
        case to_integer(currentRdLineBuffer) is
            when 0 =>
                mux_lineBufferRd(0)  <= l_rdValid ;
                mux_lineBufferRd(1)  <= l_rdValid ;
                mux_lineBufferRd(2)  <= l_rdValid ;
                mux_lineBufferRd(3)  <= '0';
            when 1 =>
                mux_lineBufferRd(0)  <= '0' ;
                mux_lineBufferRd(1)  <= l_rdValid ;
                mux_lineBufferRd(2)  <= l_rdValid ;
                mux_lineBufferRd(3)  <= l_rdValid;
            when 2 => 
                mux_lineBufferRd(0)  <= l_rdValid ;
                mux_lineBufferRd(1)  <= '0';
                mux_lineBufferRd(2)  <= l_rdValid ;
                mux_lineBufferRd(3)  <= l_rdValid;
            when 3 =>
                mux_lineBufferRd(0)  <= l_rdValid ;
                mux_lineBufferRd(1)  <= l_rdValid ;
                mux_lineBufferRd(2)  <= '0' ;
                mux_lineBufferRd(3)  <= l_rdValid;
            when others =>
                mux_lineBufferRd <= (others => '0');           
        end case;
    end if;
end process;


--mux_lineBufferWr(to_integer(currentWrLineBuffer)) <= i_data_valid;

-- Initialization of 4 line buffers -- 
lB0: entity work.entity_lineBuffer 
  port map (i_clk      => i_clk,
           i_rst       => i_rst,       
           i_data      => i_pixel_data,      
           i_wrt_valid => mux_lineBufferWr(0),
           o_lineData  => lb_data(0),
           i_rd_valid  => mux_lineBufferRd(0)
  );
 
 lB1: entity work.entity_lineBuffer 
  port map (i_clk      => i_clk,
           i_rst       => i_rst,       
           i_data      => i_pixel_data,      
           i_wrt_valid => mux_lineBufferWr(1),
           o_lineData  => lb_data(1),
           i_rd_valid  => mux_lineBufferRd(1)
  );
  
  lB2: entity work.entity_lineBuffer 
  port map (i_clk      => i_clk,
           i_rst       => i_rst,       
           i_data      => i_pixel_data,      
           i_wrt_valid => mux_lineBufferWr(2),
           o_lineData  => lb_data(2),
           i_rd_valid  => mux_lineBufferRd(2)
  );
  
  lB3: entity work.entity_lineBuffer 
  port map (i_clk      => i_clk,
           i_rst       => i_rst,       
           i_data      => i_pixel_data,      
           i_wrt_valid => mux_lineBufferWr(3),
           o_lineData  => lb_data(3),
           i_rd_valid  => mux_lineBufferRd(3)
  );

end Behavioral;
