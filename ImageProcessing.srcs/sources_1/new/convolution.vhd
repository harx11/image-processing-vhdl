----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 11/23/2021 12:48:45 PM
-- Design Name: 
-- Module Name: convolution - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------
-- BOX KERNEL is used for this module
--  1/9 [1 1 1, 1 1 1, 1 1 1]

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;


-- 2nd module 
entity entity_convolution is
  Port (i_clk         : in std_logic; 
        i_rst         : in std_logic;
        i_pixel_data  : in std_logic_vector(71 downto 0);  --output from line buffer
        i_pixel_valid : in std_logic;
        o_conv_data   : out std_logic_vector(7 downto 0);
        o_conv_valid  : out std_logic 
       );
end entity_convolution;

architecture Behavioral of entity_convolution is

    type kernel is array(8 downto 0) of std_logic_vector(7 downto 0); -- 8 bit representation for max 255 
        signal box_kernel : kernel := (others => (0 => '1', others => '0'));
     type mult_reg is array(8 downto 0) of std_logic_vector(15 downto 0); 
        signal multData : mult_reg := (others => (others => '0'));
     --signal sumData : std_logic_vector(15 downto 0) := (others => '0');
     type convOpr is (IDLE, MULT, SUM, CONV);
        signal state : convOpr := IDLE; 
     
     signal sumData :  integer := 0;


begin

mult_sum: process(i_clk)
     variable adder : integer := 0;
begin
    if rising_edge(i_clk) then
        if i_rst = '1' then
            multData <= (others => (others => '0'));
            sumData <= 0;
            o_conv_data <= (others => '0');
            state <= IDLE;
        else
        case state is
            when IDLE => 
                adder := 0; -- this should happen automatically. 
                if i_pixel_valid = '1' then
                    state <= MULT;
                end if;
            when MULT => 
                for i in 0 to 8 loop
                    multData(i) <= std_logic_vector(unsigned(box_kernel(i)) * unsigned(i_pixel_data(i*8 +7 downto i*8)));
                end loop;
                state <= SUM;
             when SUM => 
                for j in 0 to 8 loop
                    adder :=  adder + to_integer(unsigned(multData(j)));
                end loop;
                sumData <= adder; 
                state <= CONV;
             when CONV => 
                   o_conv_data <=  std_logic_vector(to_unsigned(sumData/9, o_conv_data'length));
                   o_conv_valid <= '1';
                   state <= IDLE;
                   
        end case;
     end if;        
   end if;
end process;

end Behavioral;