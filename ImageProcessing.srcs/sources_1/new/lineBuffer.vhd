----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 11/22/2021 08:58:12 PM
-- Design Name: 
-- Module Name: lineBuffer - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

--1st module 
entity entity_lineBuffer is
  Port (i_clk       : in std_logic;
       i_rst        : in std_logic;
       i_data       : in std_logic_vector(7 downto 0);
       i_wrt_valid  : in std_logic;
       o_lineData   : out std_logic_vector(23 downto 0);
       i_rd_valid   : in std_logic
  );

end entity_lineBuffer;


architecture Behavioral of entity_lineBuffer is

    type memory_reg is array(511 downto 0) of std_logic_vector (7 downto 0);
        signal line : memory_reg; --register;
    signal wrtPtr   : unsigned(8 downto 0) := (others => '0');
    signal rdPtr    : unsigned(8 downto 0) := (others => '0');

begin

-- Output data of first 3 bytes
o_lineData <= line(to_integer(rdPtr)) & line(to_integer(rdPtr+1)) & line(to_integer(rdPtr+2)); 

writingToBuffer : process(i_clk)
begin
    if rising_edge (i_clk) then
        if i_wrt_valid = '1' then 
            line(to_integer(wrtPtr)) <= i_data;
        end if;
    end if;
end process;

incrementWrtPtr: process(i_clk)
begin
    if rising_edge (i_clk) then
        if (i_rst = '1') then 
            wrtPtr <= (others => '0');
        elsif i_wrt_valid = '1' then 
            wrtPtr <= wrtPtr +1;
        end if;
    end if;    
end process;

incrementReadPtr: process(i_clk)
begin
     if rising_edge(i_clk) then 
        if i_rst = '1'  then
            rdPtr <= (others => '0');
        elsif i_rd_valid = '1' then
            rdPtr <= rdPtr + 1;
        end if;
     end if;
end process; 


end Behavioral;

