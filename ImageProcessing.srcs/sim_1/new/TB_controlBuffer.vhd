library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

entity entity_controlLogic_tb is
end;

architecture bench of entity_controlLogic_tb is


component entity_controlLogic is
     generic(KERNEL_MASK : integer := 3;    -- for a 3x3 kernel
             ACTIVE_BUFF : integer := 3;
             PIXEL_BITS  : integer := 8;
             NUM_PIXELS  : integer := 512); -- 1 line Buffer width
     Port (i_clk         : in std_logic;
           i_rst         : in std_logic;
           i_pixel_data  : in std_logic_vector(7 downto 0);
           i_data_valid  : in std_logic;
           o_pixel_data  : out std_logic_vector((KERNEL_MASK * PIXEL_BITS * ACTIVE_BUFF)-1 downto 0); -- 72 bits
           o_data_valid  : out std_logic
      );

  end component;

  signal i_clk: std_logic;
  signal i_rst: std_logic;
  signal i_pixel_data: std_logic_vector(7 downto 0);
  signal i_data_valid: std_logic;
  signal o_pixel_data: std_logic_vector(71 downto 0);
  signal o_data_valid: std_logic ;

  constant clock_period: time := 20 ns;
  signal stop_the_clock: boolean;
  signal sim_end : std_logic := '0';

begin

  -- Insert values for generic parameters !!
  uut: entity_controlLogic generic map ( kernel_mask  => 3,
                                         NUM_PIXELS   => 512 )
                              port map ( i_clk        => i_clk,
                                         i_rst        => i_rst,
                                         i_pixel_data => i_pixel_data,
                                         i_data_valid => i_data_valid,
                                         o_pixel_data => o_pixel_data,
                                         o_data_valid => o_data_valid );

  stimulus: process
  begin
     sim_end <= '0';

    -- Put test bench stimulus code here
     i_rst <= '0';
    i_data_valid <= '1';
    i_pixel_data <= x"01";
    wait for 30000 ns;
    i_pixel_data <= x"02";
    wait for 30000 ns;
    i_pixel_data <= x"04";
    wait for 30000 ns;
    i_data_valid <= '0';
    sim_end <= '1';
    wait for 10000 ns;
    

    -- stop_the_clock <= false;
    --wait;
  end process;

  clocking: process
  begin
    while not stop_the_clock loop
      i_clk <= '0', '1' after clock_period / 2;
      wait for clock_period;
    end loop;
    wait;
  end process;

end;