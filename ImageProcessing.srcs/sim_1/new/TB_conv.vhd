library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

entity entity_convolution_tb is
end;

architecture bench of entity_convolution_tb is

  component entity_convolution
    Port (i_clk         : in std_logic; 
          i_rst         : in std_logic;
          i_pixel_data  : in std_logic_vector(71 downto 0);
          i_pixel_valid : in std_logic;
          o_conv_data   : out std_logic_vector(7 downto 0);
          o_conv_valid  : out std_logic 
         );
  end component;

  signal i_clk: std_logic;
  signal i_rst: std_logic;
  signal i_pixel_data: std_logic_vector(71 downto 0);
  signal i_pixel_valid: std_logic;
  signal o_conv_data: std_logic_vector(7 downto 0);
  signal o_conv_valid: std_logic ;

  constant clock_period: time := 20 ns;
  signal stop_the_clock: boolean;

begin

  uut: entity_convolution port map ( i_clk         => i_clk,
                                     i_rst         => i_rst,
                                     i_pixel_data  => i_pixel_data,
                                     i_pixel_valid => i_pixel_valid,
                                     o_conv_data   => o_conv_data,
                                     o_conv_valid  => o_conv_valid );

  stimulus: process
  begin
  
    -- Put initialisation code here
    i_rst <= '0';
    i_pixel_data <= x"0000000000000000aa";
    i_pixel_valid <= '1';
    
    wait for 40 ns;
    i_pixel_data <= x"100000000000000aaa";

    stop_the_clock <= false;
    wait;
  end process;

  clocking: process
  begin
    while not stop_the_clock loop
      i_clk <= '0', '1' after clock_period / 2;
      wait for clock_period;
    end loop;
    wait;
  end process;

end;